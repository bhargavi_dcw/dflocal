<?php
/*
 Template Name: about
 */
get_header( 'about' ); ?>
<script>$(document).scroll(function(){var a=$(this).scrollTop();if(a>=1500){$(".client_view").addClass("fadeInUp"),1000}else{$(".client_view").removeClass("fadeInUp"),1000}});</script>
<div class="about_video">
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
<div class="video-wrapper">
<video style="width:100%;height:100%" id="video-link" autoplay loop  muted>
<source src="<?php echo the_field('about_video'); ?>" type="video/mp4">
</video>
<div class="video-btns">
<a class="btn video_play"> <i class="fa fa-pause"></i> </a>
<a class="btn video_mute"> <i class="fa fa-volume-off"></i> </a>
</div>
</div>
</div>
</div>
</div>
<div class="row video_bottom">
<div class="span-group about_ani_1">
<div id="typed-strings_about4">
<h1 style="color:#fff"><?php the_field('about_title');?></h1>
</div>
<span id="typed_about4" style="white-space:pre"></span>
</div>
<?php the_field('about_description');?>
</div>
</div>
<div class="team_details">
<div class="container-fluid">
<div class="row team">
<div class="span-group about_ani_2">
<div id="typed-strings_about5">
<h1>A TEAM OF FUN & <br> TALENTED IMAGINEERS</h1>
</div>
<span id="typed_about5" style="white-space:pre"></span>
</div>
</div>
<div class="row text-center">
<?php $y=1?>
<?php
			$Dquery = new WP_Query( array( 'post_type' => 'team' ,'order' => 'DESC') );

			while ( $Dquery->have_posts() ) : $Dquery->the_post();?>
<div class="col-md-4 col-sm-4 col-xs-4">
<div class="span-group about_ani">
<div id="typed-strings_about<?php echo $y;?>">
<h3><?php echo the_title();?></h3>
</div>
<span id="typed_about<?php echo $y;?>" style="white-space:pre"></span>
</div>

<div id="curly-brace">
<div id="left" class="brace"></div>
<div id="right" class="brace"></div>
</div>
</div>
<?php $y++; endwhile;
			?>
<?php dynamic_sidebar('group_pic');?>
</div>
</div>
</div>
<div class="description">
<div class="container-fluid">
<div class="row team_description">
<?php
			$Dquery = new WP_Query( array( 'post_type' => 'team' ) );

			while ( $Dquery->have_posts() ) : $Dquery->the_post();?>
<div class="col-md-4 col-sm-4 col-xs-4">
<div class="hvr-bob">
<?php echo the_content(); ?>
</div>
</div>
<?php endwhile;
			?>
</div>
<div class="row clients">
<?php dynamic_sidebar('love_our_work');?>
</div>
<div class="row client_view wow animated" data-wow-delay="0.8s" data-wow-offset="150" style="visibility:visible;-webkit-animation-delay:.8s;-moz-animation-delay:.8s;animation-delay:.8s">
<?php
			$query = new WP_Query( array( 'post_type' => 'testimonials','order' => 'ASC' ) );

			while ( $query->have_posts() ) : $query->the_post();?>
<div class="col-md-2 col-sm-2 col-xs-3">
<div class="view_bg hvr-bob">
<div class="view">
<h4><?php echo the_title();?></h4>
<?php echo the_content();?>
</div>
</div>
<?php the_post_thumbnail('post-thumbnail', array( 'class'	=> "img-responsive")); ?>
</div>
<?php endwhile;
			?>
</div>
</div>
</div>
<div class="contact_bottom_part">
<?php dynamic_sidebar('about_footer');?>
</div>
<?php// get_sidebar(); ?>
<?php get_footer(); ?>