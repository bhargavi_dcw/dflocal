<?php
/*
 Template Name: portfolio
 */


get_header( 'portfolio' ); ?>
<div id="portfolio_page" class="catajax">
<div class="container-fluid">
<div class="row">
<div class="col-md-12 col-sm-12 text-center">
<div class="work_typed">
<div id="typed-strings_work">
<h2 class="">I'm hosting a ...</h2>
</div>
<span id="typed_work" style="white-space:pre"></span>
</div>
<div class="clearfix"></div>
<div class="dropdown">
<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">SELECT TYPE OF EVENT
<span class="glyphicon glyphicon-chevron-down"></span></button>
<span class="event_id hide"><?= $_GET['id'];?> </span>
<?php
				$args = array(
					'type'                     => 'post',
					'child_of'                 => 0,
					'parent'                   => '',
					'orderby'                  => 'name',
					'order'                    => 'ASC',
					'hide_empty'               => 1,
					'hierarchical'             => 1,
					'exclude'                  => '',
					'include'                  => '',
					'number'                   => '',
					'post_type'                 => 'portfolio',
					'pad_counts'               => false );
				$categories = get_categories($args);
				
					echo '<ul class="dropdown-menu">';?>
<li><a onclick="get_infoajax('0')" href="javascript::void(0)">All Events</a></li>
<?php foreach ($categories as $category) {
                            //print_r($category);
                            ?>
<li><a onclick="get_infoajax(<?php echo $category->term_id; ?>,<?php if(isset($_GET['id'])){echo $_GET['id'];}else{ echo 0;}?>)" href="javascript::void(0)"><?php echo $category->name; ?></a></li>
<?php
						}
					
					echo '</ul>';
					
			?>
<div id='loadingmessage' style='display:none'>
<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
</div>
</div>
</div>
</div>
<?php   
				            $pf_query = new WP_Query(
                            array('post_type' => 'portfolio',                               
                                  'order' => 'ASC',
                                  'posts_per_page' => '-1',
								  
                                  
                            )
                        );  
				?>
<div class="row port_images">
<?php //$content = get_the_content();
			//echo "Test".$_POST['dropcatid'];
				while ($pf_query->have_posts()) : $pf_query->the_post();				
				$pflink = get_permalink( $id );
				$the_link = get_permalink();
				$title = get_the_title(); 
				$content =  get_the_content();
				$terms = get_the_terms($post->ID, 'category' );
				
			   ?>
<div class="col-md-4 col-sm-4 no_pad_port">
<div class="item">
<a href="<?php echo the_permalink();?>"><?php the_post_thumbnail('full', array('class' => 'img-responsive ports_img')); ?></a>
<span class="details">
<a href="<?php echo the_permalink();?>"><h6 class="comp_name"><?php $comp_name= the_field('company_name');?><?php echo $comp_name;?></h6></a>
<h3><a href="<?php echo the_permalink();?>"><?php echo $title;?></a></h3>
<a href="<?php echo the_permalink();?>"><h6 class="cat_name">
<?php $category = get_the_category( $post->ID );
								echo $category[0]->cat_name;?></h6></a>
</span>
</div>
</div>
<?php endwhile; 
				wp_reset_postdata();?>
</div>
</div>
</div>
<div class="port_bottom">
<?php dynamic_sidebar('work_footer');?>
</div>
<?php// get_sidebar(); ?>
<?php get_footer(); ?>