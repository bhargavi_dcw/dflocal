<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<script>
	$(function(){
			var event_id=$('.event_id').html();
			if(event_id>=1){
				get_infoajax(event_id);
			}
			
		});
    function get_infoajax(catid){
	$('#loadingmessage').show();  // show the loading message.
	$.ajax({
        type: "POST",
        url: "<?php bloginfo('template_url');?>/catajax.php",
        data: "dropcatid="+catid,
        success: function(data) {
           $("#portfolio_page").html(data);
		   $('#loadingmessage').hide(); // hide the loading message
          }
    });
    }
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.js"></script>
<script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>
<script type="text/javascript">

              jQuery.noConflict();
              jQuery('document').ready(function()
             {  
			  jQuery.validator.addMethod(
			   
								"regex",
								function(value, element, regexp) 
								{
									if (regexp.constructor != RegExp)
										regexp = new RegExp(regexp);
									else if (regexp.global)
										regexp.lastIndex = 0;
									return this.optional(element) || regexp.test(value);
								},
								"Please check your input."
						);
 
			 
                     jQuery("#register-form").validate({
                         rules: {
                             "first_name": {
								 required: true,
								 regex: /^[A-Za-z .'-]+$/,},
                             "email": {
                                 required: true,
                                 regex: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                                 //email: true
                             }, 
                         },
                         messages: {
                             "first_name": "Please enter your Name", 
                             "email": "Please enter a valid email address"
                         },
                         submitHandler: function(form) {
                             form.submit();
                         }
                     });  
                   
         });
</script>
<?php wp_footer(); ?>

</body>
</html>
