<?php
/*
 The Template for displaying all Single portfolio posts.
 */

get_header( 'portfolio' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
<?php if ( is_single() ) : ?>
<div id="portfolio_details_page">
<div class="container-fluid">
<div class="row portfolio_details_slider">
<div id="myCarousel" class="carousel slide" data-ride="carousel">
<ol class="carousel-indicators">
<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
<li data-target="#myCarousel" data-slide-to="1"></li>
<li data-target="#myCarousel" data-slide-to="2"></li>
<li data-target="#myCarousel" data-slide-to="3"></li>
</ol>
<div class="carousel-inner" role="listbox">
<?php
						$active_class = "active";
					// check if the repeater field has rows of data
					if( have_rows('slider') ):

						// loop through the rows of data
						while ( have_rows('slider') ) : the_row();
						
						$portfolio_slider = get_sub_field('slider_image');?>
<div class="item <?php echo $active_class; ?>">
<img src="<?php echo $portfolio_slider['url']; ?>" />
</div>
<?php 
                       $active_class = "";     
                      endwhile; 
                      endif; ?>
</div>
<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
<span class="sr-only">Next</span>
</a>
</div>
</div>
<div class="row detail_content">
<div class="col-md-12">
<?php 
				$args = array(
					'type'                     => 'post',
					'child_of'                 => 0,
					'parent'                   => '',
					'orderby'                  => 'name',
					'order'                    => 'ASC',
					'hide_empty'               => 1,
					'hierarchical'             => 1,
					'exclude'                  => '',
					'include'                  => '',
					'number'                   => '',
					'post_type'                 => 'portfolio',
					'pad_counts'               => false );
				$categories = get_categories($args);
				//print_r($categories);
				?>
<h3><?php $comp_name= the_field('company_name'); echo $comp_name;?></h3>
<h1><?php the_title();?></h1>
<?php 
               $category = get_the_category( $post->ID );
			   //print_r($category);
               ?>
<a href="/work/?id=<?php echo $category[0]->term_id;?>"><h5><?php echo $category[0]->cat_name;?></h5></a>
</div>
</div>
<div class="row next_project">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="next_pre_btn">
<div class="next_btnn">
<?php previous_custom_post(); ?>
</div>
<div class="project_detail">
<?php next_custom_post(); ?>
</div>
<div class="next_btnn">
<a href="/work/?id=<?php echo $category[0]->term_id;?>"><i class="fa fa-th" aria-hidden="true"></i></a>
</div>
</div>
</div>
</div>
</div>
<?php endif; // is_single() ?>
<?php endwhile; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>