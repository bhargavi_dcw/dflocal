<?php
/**
 * Twenty Sixteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentysixteen_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own twentysixteen_setup() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Twenty Sixteen, use a find and replace
	 * to change 'twentysixteen' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'twentysixteen', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for custom logo.
	 *
	 *  @since Twenty Sixteen 1.2
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 240,
		'width'       => 240,
		'flex-height' => true,
	) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'twentysixteen' ),
		'social'  => __( 'Social Links Menu', 'twentysixteen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', twentysixteen_fonts_url() ) );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // twentysixteen_setup
add_action( 'after_setup_theme', 'twentysixteen_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'twentysixteen_content_width', 840 );
}
add_action( 'after_setup_theme', 'twentysixteen_content_width', 0 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_widgets_init() {
	
	register_sidebar( array(
		'name'          => __( 'Contact Us', 'twentysixteen' ),
		'id'            => 'contact',
		'description'   => __( 'Add widgets here to appear in your Contact Us page Address', 'twentysixteen' ),
		
	) );

	register_sidebar( array(
		'name'          => __( 'About Footer', 'twentysixteen' ),
		'id'            => 'about_footer',
		'description'   => __( 'Add widgets here to appear in your About Us page Footer links', 'twentysixteen' ),
		
	) );
	register_sidebar( array(
		'name'          => __( 'Work Footer', 'twentysixteen' ),
		'id'            => 'work_footer',
		'description'   => __( 'Add widgets here to appear in your Work page Footer links', 'twentysixteen' ),
		
	) );
	register_sidebar( array(
		'name'          => __( 'Contact Footer', 'twentysixteen' ),
		'id'            => 'contact_footer',
		'description'   => __( 'Add widgets here to appear in your Contact Us page Footer links', 'twentysixteen' ),
		
	) );
	register_sidebar( array(
		'name'          => __( 'Contact Social Share', 'twentysixteen' ),
		'id'            => 'contact_social_share',
		'description'   => __( 'Add widgets here to appear in your Contact Us page Social share links', 'twentysixteen' ),
		
	) );
	register_sidebar( array(
		'name'          => __( 'Love Our Work', 'twentysixteen' ),
		'id'            => 'love_our_work',
		'description'   => __( 'Add widgets here to appear in your About Us page CLIENTS WHO LOVE OUR WORK ', 'twentysixteen' ),
		
	) );
	register_sidebar( array(
		'name'          => __( 'Team Group Pic', 'twentysixteen' ),
		'id'            => 'group_pic',
		'description'   => __( 'Add widgets here to appear in your About Us page Team Group pic ', 'twentysixteen' ),
		
	) );
	
}
add_action( 'widgets_init', 'twentysixteen_widgets_init' );

if ( ! function_exists( 'twentysixteen_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Sixteen.
 *
 * Create your own twentysixteen_fonts_url() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function twentysixteen_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'twentysixteen' ) ) {
		$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
	}

	/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'twentysixteen' ) ) {
		$fonts[] = 'Montserrat:400,700';
	}

	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentysixteen' ) ) {
		$fonts[] = 'Inconsolata:400';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentysixteen_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentysixteen-fonts', twentysixteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );

	// Theme stylesheet.
	wp_enqueue_style( 'twentysixteen-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentysixteen-style' ), '20160412' );
	wp_style_add_data( 'twentysixteen-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'twentysixteen-style' ), '20160412' );
	wp_style_add_data( 'twentysixteen-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentysixteen-style' ), '20160412' );
	wp_style_add_data( 'twentysixteen-ie7', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'twentysixteen-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'twentysixteen-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'twentysixteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20160412', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentysixteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20160412' );
	}

	wp_enqueue_script( 'twentysixteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20160412', true );

	wp_localize_script( 'twentysixteen-script', 'screenReaderText', array(
		'expand'   => __( 'expand child menu', 'twentysixteen' ),
		'collapse' => __( 'collapse child menu', 'twentysixteen' ),
	) );
}
add_action( 'wp_enqueue_scripts', 'twentysixteen_scripts' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function twentysixteen_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'twentysixteen_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function twentysixteen_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentysixteen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	840 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';

	if ( 'page' === get_post_type() ) {
		840 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	} else {
		840 > $width && 600 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		600 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentysixteen_content_image_sizes_attr', 10 , 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function twentysixteen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		! is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentysixteen_post_thumbnail_sizes_attr', 10 , 3 );

/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 *
 * @since Twenty Sixteen 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array A new modified arguments.
 */
function twentysixteen_widget_tag_cloud_args( $args ) {
	$args['largest'] = 1;
	$args['smallest'] = 1;
	$args['unit'] = 'em';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'twentysixteen_widget_tag_cloud_args' );
//Portfolio cpt registration 
function portfolio_register(){
	$args = array(
		'labels' => array(
			'name' => 'Portfolio',
			'singular_name' => 'Portfolio',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Portfolio',
			'edit_item' => 'Edit Portfolio',
			'new_item' => 'New Portfolio',
			'view_item' => 'View Portfolio',
			'search_items' => 'Search Portfolio',
			'not_found' =>  'No Portfolio found',
			'not_found_in_trash' => 'No Portfolio Entries found in Trash',
			'parent_item_colon' => ''
		),
        'taxonomies'=>array('category'),
		'singular_label' => 'portfolio',
		'public' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => false,		
		'rewrite' => array( 'slug' => 'portfolio', 'with_front' => false ),
		'query_var' => false,
		'supports' => array('title', 'editor', 'thumbnail')
	);

	register_post_type( 'Portfolio' , $args );


}
add_action('init', 'portfolio_register');

//Testimonials cpt registration 
function testimonials_register(){
	$args = array(
		'labels' => array(
			'name' => 'Testimonials',
			'singular_name' => 'Testimonials',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Testimonials',
			'edit_item' => 'Edit Testimonials',
			'new_item' => 'New Testimonials',
			'view_item' => 'View Testimonials',
			'search_items' => 'Search Testimonials',
			'not_found' =>  'No Testimonials found',
			'not_found_in_trash' => 'No Testimonials Entries found in Trash',
			'parent_item_colon' => ''
		),
        'taxonomies'=>array('category'),
		'singular_label' => 'testimonials',
		'public' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => false,		
		'rewrite' => array( 'slug' => 'testimonials', 'with_front' => false ),
		'query_var' => false,
		'supports' => array('title', 'editor', 'thumbnail')
	);

	register_post_type( 'Testimonials' , $args );


}
add_action('init', 'testimonials_register');

//Team Descriptions cpt registration 
function team_register(){
	$args = array(
		'labels' => array(
			'name' => 'Team',
			'singular_name' => 'Team',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Team',
			'edit_item' => 'Edit Team',
			'new_item' => 'New Team',
			'view_item' => 'View Team',
			'search_items' => 'Search Team',
			'not_found' =>  'No Team found',
			'not_found_in_trash' => 'No Team Entries found in Trash',
			'parent_item_colon' => ''
		),
        'taxonomies'=>array('category'),
		'singular_label' => 'team',
		'public' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => false,		
		'rewrite' => array( 'slug' => 'team', 'with_front' => false ),
		'query_var' => false,
		'supports' => array('title', 'editor', 'thumbnail')
	);

	register_post_type( 'Team' , $args );


}
add_action('init', 'team_register');

// ---------------------------------------------------------------------------------
function services_folio_meta_box($post) {
	$services_folio_post_meta = json_decode(get_post_meta($post->ID,'wp_postmeta', true));
	//print_r($services_folio_post_meta);
	$folio_head_text = $services_folio_post_meta->folio_head_text;
	$services_folio = $services_folio_post_meta->services_folio;
	?>
	<table>
	<tr>
	<td><label for="folio"><b>Portfolios</b>:</label></td>
	</tr>
	<tr>
	<td>
	<?php
	$arr_portfolios = get_posts(
	array(
	'numberposts' => -1,
	'post_status' => 'publish',
	'post_type' => 'portfolio',
	)
	);
	$svt_counter = 0;
	foreach($arr_portfolios as  $indv_portfolios)
	{
	$svt_counter++;
	$checked = (isset($services_folio) && in_array($indv_portfolios->ID, $services_folio))?' checked="checked"':'';
	echo '<div style="background:#FFFFFF;padding:3px;border:1px solid #DFDFDF;margin-bottom:3px;">
	<input type="checkbox"'. $checked .' name="services_folio[]" id="folio_'.$indv_portfolios->ID.'" value="'.$indv_portfolios->ID.'" />
	<label for="folio_'. $indv_portfolios->ID .'">'. $indv_portfolios->post_title .'</label></div>';
	}
	?>
	</td>
	</tr>
	</table>
	<?php
}
add_action('add_meta_boxes','add_services_folio_metabox');
function add_services_folio_metabox() {
	add_meta_box('servicesfoliodiv', __('Events'), 'services_folio_meta_box', 'page', 'normal', 'high');
}
add_action('save_post','services_folio_update_post',1,2);
function services_folio_update_post($post_id, $post) {
	// Get the post type. Since this function will run for ALL post saves (no matter what post type), we need to know this.
	// It's also important to note that the save_post action can runs multiple times on every post save, so you need to check and make sure the
	// post type in the passed object isn't "revision"
	$post_type = $post->post_type;
	// Logic to handle specific post types
	switch($post_type) {
	// If this is a post. You can change this case to reflect your custom post slug
	case 'page':
		$services_folio_post_meta = array();
		$services_folio_post_meta['folio_head_text'] = $_POST['folio_head_text'];
		$services_folio_post_meta['services_folio'] = $_POST['services_folio'];
		$services_folio_post_meta = json_encode($services_folio_post_meta);
		update_post_meta($post_id,'wp_postmeta', $services_folio_post_meta);
	break;
	default:
	} // End switch
	return;
}
//----------------------------------------------------------------------------------