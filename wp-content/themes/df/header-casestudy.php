<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class='no-js'>
<head>
<meta charset=<?php bloginfo( 'charset' ); ?>>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php endif; ?>
<?php wp_head(); ?>
<link href="<?php bloginfo('template_url');?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php bloginfo('template_url');?>/css/animate.css" rel="stylesheet">
<link href="<?php bloginfo('template_url');?>/css/hover.css" rel="stylesheet">
<!-- <link href="<?php //bloginfo('template_url');?>/css/examples.css" rel="stylesheet"> -->
<!-- <link href="<?php //bloginfo('template_url');?>/css/jquery.fullPage.css" rel="stylesheet"> -->
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700' rel='stylesheet' type=text/css>
<link href='https://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type=text/css>
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
<link href="https://fonts.googleapis.com/css?family=Cantarell" rel='stylesheet'>
<script src='<?php bloginfo('template_url');?>/js/jquery-3.1.0.min.js'></script>
<script src='<?php bloginfo('template_url');?>/js/typed.js'></script>
<script src='<?php bloginfo('template_url');?>/js/bootstrap.min.js'></script>
<script src='<?php bloginfo('template_url');?>/js/jquery.fullPage.js'></script>
<!-- <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js'></script> -->
<script src='<?php bloginfo('template_url');?>/js/main.js'></script>
<link rel="shortcut icon" type='image/x-icon' href='<?php bloginfo('template_url');?>/images/favicon.ico'>
<script>window.onload=downScripts;function downScripts(){var a=document.createElement("script");a.src="myScript.js";document.body.appendChild(a)};</script>
</head>
<body>
<ol class="slide_btn">
<li><a href="#" id="btn_1" class="slider_btn active"></a></li>
<li><a href="#" id="btn_2" class="slider_btn"></a></li>
<li><a href="#" id="btn_3" class="slider_btn"></a></li>
<li><a href="#" id="btn_4" class="slider_btn"></a></li>

</ol>
<div class="container-fluid">
<div class="row home_slider_menu">
<div class="col-md-6">
<a href='<?php echo esc_url( home_url( '/' ) ); ?>'><img src='<?php bloginfo('template_url');?>/images/Logo-Wheel-White.png' alt='logo' title="Design Foundary" class="home_logo"/></a>
</div>
<div class="col-md-6">
<a href="#" class="menu_btn">
<span></span>
<span></span>
<span></span>
</a>
</div>
</div>
</div>
<div id="menu_section" class="animated">
<img src='<?php bloginfo('template_url');?>/images/menu_bg.jpg' alt="" title="" class="menu_bg">
<div class="container-fluid">
<div class="row home_slider_menu">
<div class="col-md-6">
<a href='<?php echo esc_url( home_url( '/' ) ); ?>'><img src='<?php bloginfo('template_url');?>/images/Logo-Wheel-White.png' alt='logo' title="Design Foundary" class="home_logo"/></a>
</div>
<div class="col-md-6">
<div class="close-btn">
<a><span class="btn_close"></span></a>
</div>
</div>
</div>
<div class="col-md-12">
<div class="menu_list">
<?php wp_nav_menu( array('menu' => 'Main') );?>
<div class="social_icons">
<a href="https://www.facebook.com/DesignFoundryEvents/?fref=ts" target="_blank"><i class="fa fa-facebook" aria-hidden=true></i></a>
<a href=https://www.instagram.com/designfoundry/><i class="fa fa-instagram" aria-hidden=true></i></a>
<a href="#"><i class="fa fa-twitter" aria-hidden=true></i></a>
</div>
</div>
</div>
</div>
</div>
<div class="scroll">
<h5>SCROLL</h5>
<div class="clearfix">
<span class="glyphicon glyphicon-chevron-down"></span>
<span class="glyphicon glyphicon-chevron-down"></span>
</div>
</div>
<div class="main_wrapper">
<div class="wrapper">
<div id="fullpage">

