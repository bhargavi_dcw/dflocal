<?php
/*
 Template Name: contact
 */

get_header( 'about' ); ?>
<div class="contact_part">
<div class="container-fluid">
<div class="row">
<div class="span-group">
<div id="typed-strings_contact">
<h1>Let's Get To Work</h1>
</div>
<span id="typed_contact" style="white-space:pre"></span>
</div>
<div class="col-md-12">
</div>
</div>
<div class="row">
<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-center">
	<form class="form-inline" method="POST" id="register-form" action="<?php the_permalink(); ?>">
<div class="text_field">
<div class="form-group">
<p>Hello, Design Foundry. My name is
<span><input type="text" class="form-control" id="name" name="first_name" placeholder="YOUR NAME" value="" autocomplete="off" data-bv-field="first_name" required></span>
, and I'm throwing a
<input type="text" class="form-control" id="event" name="event" placeholder="EVENT TYPE" value="" autocomplete="off">
event in
<input type="text" class="form-control" id="location" placeholder="LOCATION" name="location" value="" autocomplete="off">
on
<input type="text" class="form-control" id="date" placeholder="DD/MM/YY" name="date" value="" autocomplete="off">
<!-- with a budget of
<input type="text" class="form-control" id="budget" placeholder="MAX BUDGET" name="budget" value="" autocomplete="off">
 -->. I may be reached at
<span>
<input type="text" class="form-control" id="email" placeholder="EMAIL ADDRESS" name="email" value="" autocomplete="off" required>
</span>
.</p>
</div>
</div>
<button name="submit" class="btn cheers_btn" type="submit">CHEERS!</button>
</form>
<?php
if(isset($_POST['submit']))
{
    $to="info@foundrycrew.com";
    $name = $_POST['first_name']; // required
	$event = $_POST['event']; // not required
	$email = $_POST['email']; // required
	$location = $_POST['location']; // required
	$date = $_POST['date']; // required
	//$budget = $_POST['budget'];
    $subject = "Design Foundry";

$message = <<<END

    <h2>Mr./Ms. $name has Submitted the Form</h2>
    <div style='color:#bf4a04'><b> Name:</b><strong><div style='color:#333'>$name</div></strong></div><br/>
    <div style='color:#bf4a04'><b>Event:</b><strong><div style='color:#333'>$event</div></strong></div><br/>
    <div style='color:#bf4a04'><b>Location:</b><strong><div style='color:#333'>$location</div></strong></div><br/>
	<div style='color:#bf4a04'><b>Date:</b><strong><div style='color:#333'>$date</div></strong></div><br/>
	// <div style='color:#bf4a04'><b>Budget:</b><strong><div style='color:#333'>$budget</div></strong></div><br/>
	 <div style='color:#bf4a04'><b>Email:</b><strong><div style='color:#333'>$email</div></strong></div><br/>

END;
$headers  = "From:$email\r\n";
$headers .= "Content-type: text/html\r\n";
 if(mail($to,$subject,$message,$headers,"-f $email"))
    {?>
<span class="contact_thanku"> <?php  echo 'Thanks for contacting us. You will hear from us soon';?></span>
<?php  } 
}
?>
</div>
</div>
<div class="row text-center">
</div>
</div>
<div class="container-fluid">
<div class="row">
<?php dynamic_sidebar('contact');?>
</div>
</div>
</div>
<div class="contact_logo_part">
<div class="container">
<div class="row">
<?php dynamic_sidebar('contact_social_share');?>
<div class="col-md-12">
<div class="text-center">
<h3>STAY UP TO DATE</h3>
<h5><?php dynamic_sidebar('join_our_newsletter');?></h5>
<div class="newsletter-contact">
<?php
					if( function_exists( 'mc4wp_show_form' ) ) {
						mc4wp_show_form();
					}?>
					</div>


</div>
</div>
<div class="col-md-12">
<?php dynamic_sidebar('newsletter_address');?>
</div>
</div>
</div>
</div>
<div class="contact_bottom_part">
<?php dynamic_sidebar('contact_footer')?>
</div>
<?php// get_sidebar(); ?>
<?php get_footer(); ?>