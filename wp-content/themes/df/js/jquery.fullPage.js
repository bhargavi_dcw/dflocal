/*!
 * fullPage 2.8.4
 * https://github.com/alvarotrigo/fullPage.js
 * @license MIT licensed
 *
 * Copyright (C) 2015 alvarotrigo.com - A project by Alvaro Trigo
 */
(function(b, a) {
    if (typeof define === "function" && define.amd) {
        define(["jquery"], function(c) {
            return a(c, b, b.document, b.Math)
        })
    } else {
        if (typeof exports !== "undefined") {
            module.exports = a(require("jquery"), b, b.document, b.Math)
        } else {
            a(jQuery, b, b.document, b.Math)
        }
    }
})(typeof window !== "undefined" ? window : this, function(S, aa, g, G, s) {
    var ai = "fullpage-wrapper";
    var F = "." + ai;
    var w = "fp-scrollable";
    var a = "." + w;
    var z = "fp-responsive";
    var W = "fp-notransition";
    var D = "fp-destroyed";
    var X = "fp-enabled";
    var P = "fp-viewing";
    var H = "active";
    var B = "." + H;
    var ad = "fp-completely";
    var M = "." + ad;
    var q = ".section";
    var ah = "fp-section";
    var C = "." + ah;
    var c = C + B;
    var l = C + ":first";
    var r = C + ":last";
    var b = "fp-tableCell";
    var u = "." + b;
    var ae = "fp-auto-height";
    var m = ".fp-auto-height";
    var v = "fp-normal-scroll";
    var R = ".fp-normal-scroll";
    var j = "fp-nav";
    var O = "#" + j;
    var Q = "fp-tooltip";
    var L = "." + Q;
    var ab = "fp-show-active";
    var Y = ".slide";
    var f = "fp-slide";
    var Z = "." + f;
    var k = Z + B;
    var ag = "fp-slides";
    var e = "." + ag;
    var y = "fp-slidesContainer";
    var J = "." + y;
    var N = "fp-table";
    var p = "fp-slidesNav";
    var o = "." + p;
    var d = o + " a";
    var A = "fp-controlArrow";
    var U = "." + A;
    var E = "fp-prev";
    var I = "." + E;
    var i = A + " " + E;
    var h = U + I;
    var af = "fp-next";
    var T = "." + af;
    var V = A + " " + af;
    var t = U + T;
    var ac = S(aa);
    var K = S(g);
    var n = {
        scrollbars: true,
        mouseWheel: true,
        hideScrollbars: false,
        fadeScrollbars: false,
        disableMouse: true,
        interactiveScrollbars: true
    };
    S.fn.fullpage = function(bo) {
        if (S("html").hasClass(X)) {
            b0();
            return
        }
        var bH = S("html, body");
        var a7 = S("body");
        var aG = S.fn.fullpage;
        bo = S.extend({
            menu: false,
            anchors: [],
            lockAnchors: false,
            navigation: false,
            navigationPosition: "right",
            navigationTooltips: [],
            showActiveTooltip: false,
            slidesNavigation: false,
            slidesNavPosition: "bottom",
            scrollBar: false,
            hybrid: false,
            css3: true,
            scrollingSpeed: 700,
            autoScrolling: true,
            fitToSection: true,
            fitToSectionDelay: 1000,
            easing: "easeInOutCubic",
            easingcss3: "ease",
            loopBottom: false,
            loopTop: false,
            loopHorizontal: true,
            continuousVertical: false,
            continuousHorizontal: true,
            scrollHorizontally: true,
            interlockedSlides: false,
            resetSliders: false,
            normalScrollElements: null,
            scrollOverflow: false,
            scrollOverflowHandler: x,
            scrollOverflowOptions: null,
            touchSensitivity: 5,
            normalScrollElementTouchThreshold: 5,
            bigSectionsDestination: null,
            keyboardScrolling: true,
            animateAnchor: true,
            recordHistory: true,
            controlArrows: true,
            controlArrowColor: "#fff",
            verticalCentered: true,
            sectionsColor: [],
            paddingTop: 0,
            paddingBottom: 0,
            fixedElements: null,
            responsive: 0,
            responsiveWidth: 0,
            responsiveHeight: 0,
            sectionSelector: q,
            slideSelector: Y,
            afterLoad: null,
            onLeave: null,
            afterRender: null,
            afterResize: null,
            afterReBuild: null,
            afterSlideLoad: null,
            onSlideLeave: null
        }, bo);
        var b8 = false;
        var at = navigator.userAgent.match(/(iPhone|iPod|iPad|Android|playbook|silk|BlackBerry|BB10|Windows Phone|Tizen|Bada|webOS|IEMobile|Opera Mini)/);
        var al = (("ontouchstart" in aa) || (navigator.msMaxTouchPoints > 0) || (navigator.maxTouchPoints));
        var bF = S(this);
        var ce = ac.height();
        var aT = false;
        var cg = true;
        var aH;
        var bp;
        var bA = true;
        var ak = [];
        var az;
        var aE = {};
        aE.m = {
            up: true,
            down: true,
            left: true,
            right: true
        };
        aE.k = S.extend(true, {}, aE.m);
        var aj;
        var a0;
        var bl;
        var cn;
        var bG;
        var bs;
        var bk = S.extend(true, {}, bo);
        b0();
        n.click = al;
        n = S.extend(n, bo.scrollOverflowOptions);
        S.extend(S.easing, {
            easeInOutCubic: function(cs, ct, cr, cv, cu) {
                if ((ct /= cu / 2) < 1) {
                    return cv / 2 * ct * ct * ct + cr
                }
                return cv / 2 * ((ct -= 2) * ct * ct + 2) + cr
            }
        });
        aG.setAutoScrolling = function(ct, cs) {
            b4("autoScrolling", ct, cs);
            var cr = S(c);
            if (bo.autoScrolling && !bo.scrollBar) {
                bH.css({
                    overflow: "hidden",
                    height: "100%"
                });
                aG.setRecordHistory(bk.recordHistory, "internal");
                bF.css({
                    "-ms-touch-action": "none",
                    "touch-action": "none"
                });
                if (cr.length) {
                    bM(cr.position().top)
                }
            } else {
                bH.css({
                    overflow: "visible",
                    height: "initial"
                });
                aG.setRecordHistory(false, "internal");
                bF.css({
                    "-ms-touch-action": "",
                    "touch-action": ""
                });
                bM(0);
                if (cr.length) {
                    bH.scrollTop(cr.position().top)
                }
            }
        };
        aG.setRecordHistory = function(cs, cr) {
            b4("recordHistory", cs, cr)
        };
        aG.setScrollingSpeed = function(cs, cr) {
            b4("scrollingSpeed", cs, cr)
        };
        aG.setFitToSection = function(cs, cr) {
            b4("fitToSection", cs, cr)
        };
        aG.setLockAnchors = function(cr) {
            bo.lockAnchors = cr
        };
        aG.setMouseWheelScrolling = function(cr) {
            if (cr) {
                ao();
                cl()
            } else {
                aZ();
                co()
            }
        };
        aG.setAllowScrolling = function(cr, cs) {
            if (typeof cs !== "undefined") {
                cs = cs.replace(/ /g, "").split(",");
                S.each(cs, function(ct, cu) {
                    aJ(cr, cu, "m")
                })
            } else {
                if (cr) {
                    aG.setMouseWheelScrolling(true);
                    bE()
                } else {
                    aG.setMouseWheelScrolling(false);
                    bt()
                }
            }
        };
        aG.setKeyboardScrolling = function(cr, cs) {
            if (typeof cs !== "undefined") {
                cs = cs.replace(/ /g, "").split(",");
                S.each(cs, function(ct, cu) {
                    aJ(cr, cu, "k")
                })
            } else {
                bo.keyboardScrolling = cr
            }
        };
        aG.moveSectionUp = function() {
            var cr = S(c).prev(C);
            if (!cr.length && (bo.loopTop || bo.continuousVertical)) {
                cr = S(C).last()
            }
            if (cr.length) {
                b2(cr, null, true)
            }
        };
        aG.moveSectionDown = function() {
            var cr = S(c).next(C);
            if (!cr.length && (bo.loopBottom || bo.continuousVertical)) {
                cr = S(C).first()
            }
            if (cr.length) {
                b2(cr, null, false)
            }
        };
        aG.silentMoveTo = function(cr, cs) {
            aG.setScrollingSpeed(0, "internal");
            aG.moveTo(cr, cs);
            aG.setScrollingSpeed(bk.scrollingSpeed, "internal")
        };
        aG.moveTo = function(cs, ct) {
            var cr = b7(cs);
            if (typeof ct !== "undefined") {
                an(cs, ct)
            } else {
                if (cr.length > 0) {
                    b2(cr)
                }
            }
        };
        aG.moveSlideRight = function(cr) {
            bD("right", cr)
        };
        aG.moveSlideLeft = function(cr) {
            bD("left", cr)
        };
        aG.reBuild = function(cs) {
            if (bF.hasClass(D)) {
                return
            }
            aT = true;
            ce = ac.height();
            S(C).each(function() {
                var cv = S(this).find(e);
                var cu = S(this).find(Z);
                if (bo.verticalCentered) {
                    S(this).find(u).css("height", aS(S(this)) + "px")
                }
                S(this).css("height", ce + "px");
                if (bo.scrollOverflow) {
                    if (cu.length) {
                        cu.each(function() {
                            bC(S(this))
                        })
                    } else {
                        bC(S(this))
                    }
                }
                if (cu.length > 1) {
                    bq(cv, cv.find(k))
                }
            });
            var cr = S(c);
            var ct = cr.index(C);
            if (ct) {
                aG.silentMoveTo(ct + 1)
            }
            aT = false;
            S.isFunction(bo.afterResize) && cs && bo.afterResize.call(bF);
            S.isFunction(bo.afterReBuild) && !cs && bo.afterReBuild.call(bF)
        };
        aG.setResponsive = function(cs) {
            var cr = a7.hasClass(z);
            if (cs) {
                if (!cr) {
                    aG.setAutoScrolling(false, "internal");
                    aG.setFitToSection(false, "internal");
                    S(O).hide();
                    a7.addClass(z)
                }
            } else {
                if (cr) {
                    aG.setAutoScrolling(bk.autoScrolling, "internal");
                    aG.setFitToSection(bk.autoScrolling, "internal");
                    S(O).show();
                    a7.removeClass(z)
                }
            }
        };
        aG.getFullpageData = function() {
            return {
                options: bo,
                internals: {
                    getXmovement: bW,
                    removeAnimation: ca,
                    getTransforms: bJ,
                    lazyLoad: bm,
                    addAnimation: bg,
                    performHorizontalMove: aV,
                    silentLandscapeScroll: a5
                }
            }
        };
        if (S(this).length) {
            aB("fp_continuousHorizontalExtension");
            aB("fp_scrollHorizontallyExtension");
            aB("fp_resetSlidersExtension");
            aB("fp_interlockedSlidesExtension");
            bu();
            bX()
        }

        function bu() {
            if (bo.css3) {
                bo.css3 = bx()
            }
            bo.scrollBar = bo.scrollBar || bo.hybrid;
            bO();
            a3();
            aG.setAllowScrolling(true);
            aG.setAutoScrolling(bo.autoScrolling, "internal");
            var cr = S(c).find(k);
            if (cr.length && (S(c).index(C) !== 0 || (S(c).index(C) === 0 && cr.index() !== 0))) {
                a5(cr)
            }
            ch();
            aX();
            if (g.readyState === "complete") {
                ci()
            }
            ac.on("load", ci)
        }

        function bX() {
            ac.on("scroll", bT).on("hashchange", aI).blur(br).resize(aQ);
            K.keydown(b5).keyup(aU).on("click touchstart", O + " a", b9).on("click touchstart", d, bB).on("click", L, bh);
            S(C).on("click touchstart", U, cj);
            if (bo.normalScrollElements) {
                K.on("mouseenter", bo.normalScrollElements, function() {
                    aG.setMouseWheelScrolling(false)
                });
                K.on("mouseleave", bo.normalScrollElements, function() {
                    aG.setMouseWheelScrolling(true)
                })
            }
        }

        function aB(cr) {
            var cs = cr.replace("fp_", "").replace("Extension", "");
            aG[cs] = typeof aa[cr] !== "undefined" ? new aa[cr]() : null
        }

        function bO() {
            var cr = bF.find(bo.sectionSelector);
            if (!bo.anchors.length) {
                bo.anchors = cr.filter("[data-anchor]").map(function() {
                    return S(this).data("anchor").toString()
                }).get()
            }
            if (!bo.navigationTooltips.length) {
                bo.navigationTooltips = cr.filter("[data-tooltip]").map(function() {
                    return S(this).data("tooltip").toString()
                }).get()
            }
        }

        function a3() {
            bF.css({
                height: "100%",
                position: "relative"
            });
            bF.addClass(ai);
            S("html").addClass(X);
            ce = ac.height();
            bF.removeClass(D);
            a1();
            S(C).each(function(cr) {
                var cu = S(this);
                var ct = cu.find(Z);
                var cs = ct.length;
                bd(cu, cr);
                by(cu, cr);
                if (cs > 0) {
                    aq(cu, ct, cs)
                } else {
                    if (bo.verticalCentered) {
                        bf(cu)
                    }
                }
            });
            if (bo.fixedElements && bo.css3) {
                S(bo.fixedElements).appendTo(a7)
            }
            if (bo.navigation) {
                be()
            }
            bi();
            cb();
            if (bo.scrollOverflow) {
                if (g.readyState === "complete") {
                    aK()
                }
                ac.on("load", aK)
            } else {
                bz()
            }
        }

        function aq(cv, ct, cs) {
            var cu = cs * 100;
            var cw = 100 / cs;
            ct.wrapAll('<div class="' + y + '" />');
            ct.parent().wrap('<div class="' + ag + '" />');
            cv.find(J).css("width", cu + "%");
            if (cs > 1) {
                if (bo.controlArrows) {
                    av(cv)
                }
                if (bo.slidesNavigation) {
                    aP(cv, cs)
                }
            }
            ct.each(function(cx) {
                S(this).css("width", cw + "%");
                if (bo.verticalCentered) {
                    bf(S(this))
                }
            });
            var cr = cv.find(k);
            if (cr.length && (S(c).index(C) !== 0 || (S(c).index(C) === 0 && cr.index() !== 0))) {
                a5(cr)
            } else {
                ct.eq(0).addClass(H)
            }
        }

        function bd(cs, cr) {
            if (!cr && S(c).length === 0) {
                cs.addClass(H)
            }
            cs.css("height", ce + "px");
            if (bo.paddingTop) {
                cs.css("padding-top", bo.paddingTop)
            }
            if (bo.paddingBottom) {
                cs.css("padding-bottom", bo.paddingBottom)
            }
            if (typeof bo.sectionsColor[cr] !== "undefined") {
                cs.css("background-color", bo.sectionsColor[cr])
            }
            if (typeof bo.anchors[cr] !== "undefined") {
                cs.attr("data-anchor", bo.anchors[cr])
            }
        }

        function by(cs, cr) {
            if (typeof bo.anchors[cr] !== "undefined") {
                if (cs.hasClass(H)) {
                    aM(bo.anchors[cr], cr)
                }
            }
            if (bo.menu && bo.css3 && S(bo.menu).closest(F).length) {
                S(bo.menu).appendTo(a7)
            }
        }

        function a1() {
            bF.find(bo.sectionSelector).addClass(ah);
            bF.find(bo.slideSelector).addClass(f)
        }

        function av(cr) {
            cr.find(e).after('<div class="' + i + '"></div><div class="' + V + '"></div>');
            if (bo.controlArrowColor != "#fff") {
                cr.find(t).css("border-color", "transparent transparent transparent " + bo.controlArrowColor);
                cr.find(h).css("border-color", "transparent " + bo.controlArrowColor + " transparent transparent")
            }
            if (!bo.loopHorizontal) {
                cr.find(h).hide()
            }
        }

        function be() {
            a7.append('<div id="' + j + '"><ul></ul></div>');
            var cv = S(O);
            cv.addClass(function() {
                return bo.showActiveTooltip ? ab + " " + bo.navigationPosition : bo.navigationPosition
            });
            for (var cs = 0; cs < S(C).length; cs++) {
                var ct = "";
                if (bo.anchors.length) {
                    ct = bo.anchors[cs]
                }
                var cr = '<li><a href="#' + ct + '"><span></span></a>';
                var cu = bo.navigationTooltips[cs];
                if (typeof cu !== "undefined" && cu !== "") {
                    cr += '<div class="' + Q + " " + bo.navigationPosition + '">' + cu + "</div>"
                }
                cr += "</li>";
                cv.find("ul").append(cr)
            }
            S(O).css("margin-top", "-" + (S(O).height() / 2) + "px");
            S(O).find("li").eq(S(c).index(C)).find("a").addClass(H)
        }

        function aK() {
            S(C).each(function() {
                var cr = S(this).find(Z);
                if (cr.length) {
                    cr.each(function() {
                        bC(S(this))
                    })
                } else {
                    bC(S(this))
                }
            });
            bz()
        }

        function bi() {
            bF.find('iframe[src*="youtube.com/embed/"]').each(function() {
                bj(S(this), "enablejsapi=1")
            })
        }

        function cb() {
            bF.find('iframe[src*="player.vimeo.com/"]').each(function() {
                bj(S(this), "api=1")
            })
        }

        function bj(cs, ct) {
            var cr = cs.attr("src");
            cs.attr("src", cr + bI(cr) + ct)
        }

        function bI(cr) {
            return (!/\?/.test(cr)) ? "?" : "&"
        }

        function bz() {
            var cr = S(c);
            cr.addClass(ad);
            if (bo.scrollOverflowHandler.afterRender) {
                bo.scrollOverflowHandler.afterRender(cr)
            }
            bm(cr);
            b1(cr);
            S.isFunction(bo.afterLoad) && bo.afterLoad.call(cr, cr.data("anchor"), (cr.index(C) + 1));
            S.isFunction(bo.afterRender) && bo.afterRender.call(bF)
        }
        var aA = false;
        var aR = 0;

        function bT() {
            var cr;
            if (!bo.autoScrolling || bo.scrollBar) {
                var cv = ac.scrollTop();
                var cs = bS(cv);
                var cz = 0;
                var cD = cv + (ac.height() / 2);
                var cw = a7.height() - ac.height() === cv;
                var cH = g.querySelectorAll(C);
                if (cw) {
                    cz = cH.length - 1
                } else {
                    for (var cx = 0; cx < cH.length; ++cx) {
                        var cE = cH[cx];
                        if (cE.offsetTop <= cD) {
                            cz = cx
                        }
                    }
                }
                if (bQ(cs)) {
                    if (!S(c).hasClass(ad)) {
                        S(c).addClass(ad).siblings().removeClass(ad)
                    }
                }
                cr = S(cH).eq(cz);
                if (!cr.hasClass(H)) {
                    aA = true;
                    var ct = S(c);
                    var cG = ct.index(C) + 1;
                    var cy = aD(cr);
                    var cu = cr.data("anchor");
                    var cC = cr.index(C) + 1;
                    var cF = cr.find(k);
                    if (cF.length) {
                        var cA = cF.data("anchor");
                        var cB = cF.index()
                    }
                    if (bA) {
                        cr.addClass(H).siblings().removeClass(H);
                        S.isFunction(bo.onLeave) && bo.onLeave.call(ct, cG, cC, cy);
                        S.isFunction(bo.afterLoad) && bo.afterLoad.call(cr, cu, cC);
                        aC(ct);
                        bm(cr);
                        b1(cr);
                        aM(cu, cC - 1);
                        if (bo.anchors.length) {
                            aH = cu
                        }
                        bL(cB, cA, cu, cC)
                    }
                    clearTimeout(cn);
                    cn = setTimeout(function() {
                        aA = false
                    }, 100)
                }
                if (bo.fitToSection) {
                    clearTimeout(bG);
                    bG = setTimeout(function() {
                        if (bA && bo.fitToSection) {
                            if (S(c).is(cr)) {
                                aT = true
                            }
                            b2(S(c));
                            aT = false
                        }
                    }, bo.fitToSectionDelay)
                }
            }
        }

        function bQ(cs) {
            var ct = S(c).position().top;
            var cr = ct + ac.height();
            if (cs == "up") {
                return cr >= (ac.scrollTop() + ac.height())
            }
            return ct <= ac.scrollTop()
        }

        function bS(cr) {
            var cs = cr > aR ? "down" : "up";
            aR = cr;
            a6 = cr;
            return cs
        }

        function bZ(ct, cu) {
            if (!aE.m[ct]) {
                return
            }
            var cr = (ct === "down") ? "bottom" : "top";
            var cs = (ct === "down") ? aG.moveSectionDown : aG.moveSectionUp;
            if (aG.scrollHorizontally) {
                cs = aG.scrollHorizontally.getScrollSection(ct, cs)
            }
            if (cu.length > 0) {
                if (bo.scrollOverflowHandler.isScrolled(cr, cu)) {
                    cs()
                } else {
                    return true
                }
            } else {
                cs()
            }
        }
        var bU = 0;
        var bV = 0;
        var bN = 0;
        var bP = 0;

        function b3(ct) {
            var cv = ct.originalEvent;
            var cr = S(cv.target).closest(C);
            if (!ax(ct.target) && cc(cv)) {
                if (bo.autoScrolling) {
                    ct.preventDefault()
                }
                var cu = bo.scrollOverflowHandler.scrollable(cr);
                if (bA && !b8) {
                    var cs = am(cv);
                    bN = cs.y;
                    bP = cs.x;
                    if (cr.find(e).length && G.abs(bV - bP) > (G.abs(bU - bN))) {
                        if (G.abs(bV - bP) > (ac.outerWidth() / 100 * bo.touchSensitivity)) {
                            if (bV > bP) {
                                if (aE.m.right) {
                                    aG.moveSlideRight(cr)
                                }
                            } else {
                                if (aE.m.left) {
                                    aG.moveSlideLeft(cr)
                                }
                            }
                        }
                    } else {
                        if (bo.autoScrolling) {
                            if (G.abs(bU - bN) > (ac.height() / 100 * bo.touchSensitivity)) {
                                if (bU > bN) {
                                    bZ("down", cu)
                                } else {
                                    if (bN > bU) {
                                        bZ("up", cu)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        function ax(ct, cr) {
            cr = cr || 0;
            var cs = S(ct).parent();
            if (cr < bo.normalScrollElementTouchThreshold && cs.is(bo.normalScrollElements)) {
                return true
            } else {
                if (cr == bo.normalScrollElementTouchThreshold) {
                    return false
                } else {
                    return ax(cs, ++cr)
                }
            }
        }

        function cc(cr) {
            return typeof cr.pointerType === "undefined" || cr.pointerType != "mouse"
        }

        function cp(cs) {
            var ct = cs.originalEvent;
            if (bo.fitToSection) {
                bH.stop()
            }
            if (cc(ct)) {
                var cr = am(ct);
                bU = cr.y;
                bV = cr.x
            }
        }

        function a2(cv, cu) {
            var ct = 0;
            var cr = cv.slice(G.max(cv.length - cu, 1));
            for (var cs = 0; cs < cr.length; cs++) {
                ct = ct + cr[cs]
            }
            return G.ceil(ct / cu)
        }
        var ar = new Date().getTime();

        function a8(cx) {
            var cu = new Date().getTime();
            var cz = S(M).hasClass(v);
            if (bo.autoScrolling && !az && !cz) {
                cx = cx || aa.event;
                var cB = cx.wheelDelta || -cx.deltaY || -cx.detail;
                var cC = G.max(-1, G.min(1, cB));
                var cy = typeof cx.wheelDeltaX !== "undefined" || typeof cx.deltaX !== "undefined";
                var ct = (G.abs(cx.wheelDeltaX) < G.abs(cx.wheelDelta)) || (G.abs(cx.deltaX) < G.abs(cx.deltaY) || !cy);
                if (ak.length > 149) {
                    ak.shift()
                }
                ak.push(G.abs(cB));
                if (bo.scrollBar) {
                    cx.preventDefault ? cx.preventDefault() : cx.returnValue = false
                }
                var cD = S(c);
                var cA = bo.scrollOverflowHandler.scrollable(cD);
                var cr = cu - ar;
                ar = cu;
                if (cr > 200) {
                    ak = []
                }
                if (bA) {
                    var cw = a2(ak, 10);
                    var cs = a2(ak, 70);
                    var cv = cw >= cs;
                    if (cv && ct) {
                        if (cC < 0) {
                            bZ("down", cA)
                        } else {
                            bZ("up", cA)
                        }
                    }
                }
                return false
            }
            if (bo.fitToSection) {
                bH.stop()
            }
        }

        function bD(cw, cv) {
            var cr = typeof cv === "undefined" ? S(c) : cv;
            var cu = cr.find(e);
            var ct = cu.find(Z).length;
            if (!cu.length || b8 || ct < 2) {
                return
            }
            var cx = cu.find(k);
            var cs = null;
            if (cw === "left") {
                cs = cx.prev(Z)
            } else {
                cs = cx.next(Z)
            }
            if (!cs.length) {
                if (!bo.loopHorizontal) {
                    return
                }
                if (cw === "left") {
                    cs = cx.siblings(":last")
                } else {
                    cs = cx.siblings(":first")
                }
            }
            b8 = true;
            bq(cu, cs, cw)
        }

        function bw() {
            S(k).each(function() {
                a5(S(this), "internal")
            })
        }
        var a6 = 0;

        function a9(cu) {
            var ct = cu.position();
            var cr = ct.top;
            var cw = ct.top > a6;
            var cv = cr - ce + cu.outerHeight();
            var cs = bo.bigSectionsDestination;
            if (cu.outerHeight() > ce) {
                if (!cw && !cs || cs === "bottom") {
                    cr = cv
                }
            } else {
                if (cw || (aT && cu.is(":last-child"))) {
                    cr = cv
                }
            }
            a6 = cr;
            return cr
        }

        function b2(ct, cx, cs) {
            if (typeof ct === "undefined") {
                return
            }
            var cu = a9(ct);
            var cr = {
                element: ct,
                callback: cx,
                isMovementUp: cs,
                dtop: cu,
                yMovement: aD(ct),
                anchorLink: ct.data("anchor"),
                sectionIndex: ct.index(C),
                activeSlide: ct.find(k),
                activeSection: S(c),
                leavingSection: S(c).index(C) + 1,
                localIsResizing: aT
            };
            if ((cr.activeSection.is(ct) && !aT) || (bo.scrollBar && ac.scrollTop() === cr.dtop && !ct.hasClass(ae))) {
                return
            }
            if (cr.activeSlide.length) {
                var cw = cr.activeSlide.data("anchor");
                var cv = cr.activeSlide.index()
            }
            if (bo.autoScrolling && bo.continuousVertical && typeof(cr.isMovementUp) !== "undefined" && ((!cr.isMovementUp && cr.yMovement == "up") || (cr.isMovementUp && cr.yMovement == "down"))) {
                cr = cf(cr)
            }
            if (S.isFunction(bo.onLeave) && !cr.localIsResizing) {
                if (bo.onLeave.call(cr.activeSection, cr.leavingSection, (cr.sectionIndex + 1), cr.yMovement) === false) {
                    return
                }
            }
            aC(cr.activeSection);
            ct.addClass(H).siblings().removeClass(H);
            bm(ct);
            bo.scrollOverflowHandler.onLeave();
            bA = false;
            bL(cv, cw, cr.anchorLink, cr.sectionIndex);
            bR(cr);
            aH = cr.anchorLink;
            aM(cr.anchorLink, cr.sectionIndex)
        }

        function bR(cr) {
            if (bo.css3 && bo.autoScrolling && !bo.scrollBar) {
                var cs = "translate3d(0px, -" + cr.dtop + "px, 0px)";
                bv(cs, true);
                if (bo.scrollingSpeed) {
                    a0 = setTimeout(function() {
                        b6(cr)
                    }, bo.scrollingSpeed)
                } else {
                    b6(cr)
                }
            } else {
                var ct = aO(cr);
                S(ct.element).animate(ct.options, bo.scrollingSpeed, bo.easing).promise().done(function() {
                    if (bo.scrollBar) {
                        setTimeout(function() {
                            b6(cr)
                        }, 30)
                    } else {
                        b6(cr)
                    }
                })
            }
        }

        function aO(cs) {
            var cr = {};
            if (bo.autoScrolling && !bo.scrollBar) {
                cr.options = {
                    top: -cs.dtop
                };
                cr.element = F
            } else {
                cr.options = {
                    scrollTop: cs.dtop
                };
                cr.element = "html, body"
            }
            return cr
        }

        function cf(cr) {
            if (!cr.isMovementUp) {
                S(c).after(cr.activeSection.prevAll(C).get().reverse())
            } else {
                S(c).before(cr.activeSection.nextAll(C))
            }
            bM(S(c).position().top);
            bw();
            cr.wrapAroundElements = cr.activeSection;
            cr.dtop = cr.element.position().top;
            cr.yMovement = aD(cr.element);
            return cr
        }

        function a4(cr) {
            if (!cr.wrapAroundElements || !cr.wrapAroundElements.length) {
                return
            }
            if (cr.isMovementUp) {
                S(l).before(cr.wrapAroundElements)
            } else {
                S(r).after(cr.wrapAroundElements)
            }
            bM(S(c).position().top);
            bw()
        }

        function b6(cr) {
            a4(cr);
            S.isFunction(bo.afterLoad) && !cr.localIsResizing && bo.afterLoad.call(cr.element, cr.anchorLink, (cr.sectionIndex + 1));
            bo.scrollOverflowHandler.afterLoad();
            if (bo.resetSliders && aG.resetSliders) {
                aG.resetSliders.apply(cr)
            }
            b1(cr.element);
            cr.element.addClass(ad).siblings().removeClass(ad);
            bA = true;
            S.isFunction(cr.callback) && cr.callback.call(this)
        }

        function bm(cr) {
            var cr = ck(cr);
            cr.find("img[data-src], source[data-src], audio[data-src], iframe[data-src]").each(function() {
                S(this).attr("src", S(this).data("src"));
                S(this).removeAttr("data-src");
                if (S(this).is("source")) {
                    S(this).closest("video").get(0).load()
                }
            })
        }

        function b1(cr) {
            var cr = ck(cr);
            cr.find("video, audio").each(function() {
                var cs = S(this).get(0);
                if (cs.hasAttribute("data-autoplay") && typeof cs.play === "function") {
                    cs.play()
                }
            });
            cr.find('iframe[src*="youtube.com/embed/"]').each(function() {
                var cs = S(this).get(0);
                bn(cs);
                cs.onload = function() {
                    bn(cs)
                }
            })
        }

        function bn(cr) {
            cr.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', "*")
        }

        function aC(cr) {
            var cr = ck(cr);
            cr.find("video, audio").each(function() {
                var cs = S(this).get(0);
                if (!cs.hasAttribute("data-keepplaying") && typeof cs.pause === "function") {
                    //cs.pause()
                }
            });
            cr.find('iframe[src*="youtube.com/embed/"]').each(function() {
                var cs = S(this).get(0);
                if (/youtube\.com\/embed\//.test(S(this).attr("src")) && !cs.hasAttribute("data-keepplaying")) {
                    S(this).get(0).contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', "*")
                }
            })
        }

        function ck(cs) {
            var cr = cs.find(k);
            if (cr.length) {
                cs = S(cr)
            }
            return cs
        }

        function ci() {
            var cs = aa.location.hash.replace("#", "").split("/");
            var ct = decodeURIComponent(cs[0]);
            var cr = decodeURIComponent(cs[1]);
            if (ct) {
                if (bo.animateAnchor) {
                    an(ct, cr)
                } else {
                    aG.silentMoveTo(ct, cr)
                }
            }
        }

        function aI() {
            if (!aA && !bo.lockAnchors) {
                var ct = aa.location.hash.replace("#", "").split("/");
                var cv = decodeURIComponent(ct[0]);
                var cr = decodeURIComponent(ct[1]);
                var cu = (typeof aH === "undefined");
                var cs = (typeof aH === "undefined" && typeof cr === "undefined" && !b8);
                if (cv.length) {
                    if ((cv && cv !== aH) && !cu || cs || (!b8 && bp != cr)) {
                        an(cv, cr)
                    }
                }
            }
        }

        function b5(cu) {
            clearTimeout(bs);
            var cr = S(":focus");
            if (!cr.is("textarea") && !cr.is("input") && !cr.is("select") && cr.attr("contentEditable") !== "true" && cr.attr("contentEditable") !== "" && bo.keyboardScrolling && bo.autoScrolling) {
                var ct = cu.which;
                var cs = [40, 38, 32, 33, 34];
                if (S.inArray(ct, cs) > -1) {
                    cu.preventDefault()
                }
                az = cu.ctrlKey;
                bs = setTimeout(function() {
                    bc(cu)
                }, 150)
            }
        }

        function bh() {
            S(this).prev().trigger("click")
        }

        function aU(cr) {
            if (cg) {
                az = cr.ctrlKey
            }
        }

        function ap(cr) {
            if (cr.which == 2) {
                bb = cr.pageY;
                bF.on("mousemove", cm)
            }
        }

        function aN(cr) {
            if (cr.which == 2) {
                bF.off("mousemove")
            }
        }

        function cj() {
            var cr = S(this).closest(C);
            if (S(this).hasClass(E)) {
                if (aE.m.left) {
                    aG.moveSlideLeft(cr)
                }
            } else {
                if (aE.m.right) {
                    aG.moveSlideRight(cr)
                }
            }
        }

        function br() {
            cg = false;
            az = false
        }

        function b9(cs) {
            cs.preventDefault();
            var cr = S(this).parent().index();
            b2(S(C).eq(cr))
        }

        function bB(ct) {
            ct.preventDefault();
            var cs = S(this).closest(C).find(e);
            var cr = cs.find(Z).eq(S(this).closest("li").index());
            bq(cs, cr)
        }

        function bc(cs) {
            var cr = cs.shiftKey;
            switch (cs.which) {
                case 38:
                case 33:
                    if (aE.k.up) {
                        aG.moveSectionUp()
                    }
                    break;
                case 32:
                    if (cr && aE.k.up) {
                        aG.moveSectionUp();
                        break
                    }
                case 40:
                case 34:
                    if (aE.k.down) {
                        aG.moveSectionDown()
                    }
                    break;
                case 36:
                    if (aE.k.up) {
                        aG.moveTo(1)
                    }
                    break;
                case 35:
                    if (aE.k.down) {
                        aG.moveTo(S(C).length)
                    }
                    break;
                case 37:
                    if (aE.k.left) {
                        aG.moveSlideLeft()
                    }
                    break;
                case 39:
                    if (aE.k.right) {
                        aG.moveSlideRight()
                    }
                    break;
                default:
                    return
            }
        }
        var bb = 0;

        function cm(cr) {
            if (bA) {
                if (cr.pageY < bb && aE.m.up) {
                    aG.moveSectionUp()
                } else {
                    if (cr.pageY > bb && aE.m.down) {
                        aG.moveSectionDown()
                    }
                }
            }
            bb = cr.pageY
        }

        function bq(ct, cs, cv) {
            var cu = ct.closest(C);
            var cr = {
                slides: ct,
                destiny: cs,
                direction: cv,
                destinyPos: cs.position(),
                slideIndex: cs.index(),
                section: cu,
                sectionIndex: cu.index(C),
                anchorLink: cu.data("anchor"),
                slidesNav: cu.find(o),
                slideAnchor: bY(cs),
                prevSlide: cu.find(k),
                prevSlideIndex: cu.find(k).index(),
                localIsResizing: aT
            };
            cr.xMovement = bW(cr.prevSlideIndex, cr.slideIndex);
            bA = false;
            if (bo.onSlideLeave) {
                if (!cr.localIsResizing && cr.xMovement !== "none") {
                    if (S.isFunction(bo.onSlideLeave)) {
                        if (bo.onSlideLeave.call(cr.prevSlide, cr.anchorLink, (cr.sectionIndex + 1), cr.prevSlideIndex, cr.xMovement, cr.slideIndex) === false) {
                            b8 = false;
                            return
                        }
                    }
                }
            }
            aC(cr.prevSlide);
            cs.addClass(H).siblings().removeClass(H);
            if (!cr.localIsResizing) {
                bm(cs)
            }
            if (!bo.loopHorizontal && bo.controlArrows) {
                cu.find(h).toggle(cr.slideIndex !== 0);
                cu.find(t).toggle(!cs.is(":last-child"))
            }
            if (cu.hasClass(H)) {
                bL(cr.slideIndex, cr.slideAnchor, cr.anchorLink, cr.sectionIndex)
            }
            if (aG.continuousHorizontal) {
                aG.continuousHorizontal.apply(cr)
            }
            aV(ct, cr, true);
            if (bo.interlockedSlides && aG.interlockedSlides) {
                aG.interlockedSlides.apply(cr)
            }
        }

        function bK(cr) {
            if (aG.continuousHorizontal) {
                aG.continuousHorizontal.afterSlideLoads(cr)
            }
            aY(cr.slidesNav, cr.slideIndex);
            if (!cr.localIsResizing) {
                S.isFunction(bo.afterSlideLoad) && bo.afterSlideLoad.call(cr.destiny, cr.anchorLink, (cr.sectionIndex + 1), cr.slideAnchor, cr.slideIndex);
                bA = true
            }
            b1(cr.destiny);
            b8 = false;
            if (aG.interlockedSlides) {
                aG.interlockedSlides.apply(cr)
            }
        }

        function aV(cv, ct, cs) {
            var cr = ct.destinyPos;
            if (bo.css3) {
                var cu = "translate3d(-" + G.round(cr.left) + "px, 0px, 0px)";
                bg(cv.find(J)).css(bJ(cu));
                bl = setTimeout(function() {
                    cs && bK(ct)
                }, bo.scrollingSpeed, bo.easing)
            } else {
                cv.animate({
                    scrollLeft: G.round(cr.left)
                }, bo.scrollingSpeed, bo.easing, function() {
                    cs && bK(ct)
                })
            }
        }

        function aY(cr, cs) {
            cr.find(B).removeClass(H);
            cr.find("li").eq(cs).find("a").addClass(H)
        }
        var cq = ce;

        function aQ() {
            ch();
            if (at) {
                var cr = S(g.activeElement);
                if (!cr.is("textarea") && !cr.is("input") && !cr.is("select")) {
                    var cs = ac.height();
                    if (G.abs(cs - cq) > (20 * G.max(cq, cs) / 100)) {
                        aG.reBuild(true);
                        cq = cs
                    }
                }
            } else {
                clearTimeout(aj);
                aj = setTimeout(function() {
                    aG.reBuild(true)
                }, 350)
            }
        }

        function ch() {
            var cr = bo.responsive || bo.responsiveWidth;
            var cu = bo.responsiveHeight;
            var ct = cr && ac.outerWidth() < cr;
            var cs = cu && ac.height() < cu;
            if (cr && cu) {
                aG.setResponsive(ct || cs)
            } else {
                if (cr) {
                    aG.setResponsive(ct)
                } else {
                    if (cu) {
                        aG.setResponsive(cs)
                    }
                }
            }
        }

        function bg(cr) {
            var cs = "all " + bo.scrollingSpeed + "ms " + bo.easingcss3;
            cr.removeClass(W);
            return cr.css({
                "-webkit-transition": cs,
                transition: cs
            })
        }

        function ca(cr) {
            return cr.addClass(W)
        }

        function aW(cr, cs) {
            if (bo.navigation) {
                S(O).find(B).removeClass(H);
                if (cr) {
                    S(O).find('a[href="#' + cr + '"]').addClass(H)
                } else {
                    S(O).find("li").eq(cs).find("a").addClass(H)
                }
            }
        }

        function ba(cr) {
            if (bo.menu) {
                S(bo.menu).find(B).removeClass(H);
                S(bo.menu).find('[data-menuanchor="' + cr + '"]').addClass(H)
            }
        }

        function aM(ct, cs) {
            var cr = cs + 1;
            if (cr == 1) {
                var cu = S("#videobg")[0];
                if (cu.paused) {
                    cu.play()
                }
            }
            if (cr == 6) {
                var cu = S("#videobglast")[0];
                if (cu.paused) {
                    cu.play()
                }
            }
            S(".slide_btn .slider_btn").removeClass("active");
            S(".slide_btn #btn_" + cr).addClass("active");
            S("#typed_" + cr).typed({
                stringsElement: S("#typed-strings_" + cr),
                typeSpeed: 5,
                backDelay: 3000,
                loop: true,
                contentType: "html",
                loopCount: false,
                callback: function() {
                    cw()
                },
                resetCallback: function() {
                    cv()
                }
            });
            S(".reset").click(function() {
                S("#typed_" + cr).typed("reset")
            });

            function cv() {}

            function cw() {
                console.log("Callback")
            }
            ba(ct);
            aW(ct, cs)
        }

        function aD(cs) {
            var cr = S(c).index(C);
            var ct = cs.index(C);
            if (cr == ct) {
                return "none"
            }
            if (cr > ct) {
                return "up"
            }
            return "down"
        }

        function bW(cr, cs) {
            if (cr == cs) {
                return "none"
            }
            if (cr > cs) {
                return "left"
            }
            return "right"
        }

        function bC(cs) {
            if (cs.hasClass("fp-noscroll")) {
                return
            }
            cs.css("overflow", "hidden");
            var cr = bo.scrollOverflowHandler;
            var cu = cr.wrapContent();
            var cx = cs.closest(C);
            var cw = cr.scrollable(cs);
            var cv;
            if (cw.length) {
                cv = cr.scrollHeight(cs)
            } else {
                cv = cs.get(0).scrollHeight;
                if (bo.verticalCentered) {
                    cv = cs.find(u).get(0).scrollHeight
                }
            }
            var ct = ce - parseInt(cx.css("padding-bottom")) - parseInt(cx.css("padding-top"));
            if (cv > ct) {
                if (cw.length) {
                    cr.update(cs, ct)
                } else {
                    if (bo.verticalCentered) {
                        cs.find(u).wrapInner(cu)
                    } else {
                        cs.wrapInner(cu)
                    }
                    cr.create(cs, ct)
                }
            } else {
                cr.remove(cs)
            }
            cs.css("overflow", "")
        }

        function bf(cr) {
            cr.addClass(N).wrapInner('<div class="' + b + '" style="height:' + aS(cr) + 'px;" />')
        }

        function aS(cs) {
            var ct = ce;
            if (bo.paddingTop || bo.paddingBottom) {
                var cu = cs;
                if (!cu.hasClass(ah)) {
                    cu = cs.closest(C)
                }
                var cr = parseInt(cu.css("padding-top")) + parseInt(cu.css("padding-bottom"));
                ct = (ce - cr)
            }
            return ct
        }

        function bv(cr, cs) {
            if (cs) {
                bg(bF)
            } else {
                ca(bF)
            }
            bF.css(bJ(cr));
            setTimeout(function() {
                bF.removeClass(W)
            }, 10)
        }

        function b7(cr) {
            var cs = bF.find(C + '[data-anchor="' + cr + '"]');
            if (!cs.length) {
                cs = S(C).eq((cr - 1))
            }
            return cs
        }

        function au(cu, ct) {
            var cs = ct.find(e);
            var cr = cs.find(Z + '[data-anchor="' + cu + '"]');
            if (!cr.length) {
                cr = cs.find(Z).eq(cu)
            }
            return cr
        }

        function an(cs, cr) {
            var ct = b7(cs);
            if (typeof cr === "undefined") {
                cr = 0
            }
            if (cs !== aH && !ct.hasClass(H)) {
                b2(ct, function() {
                    aL(ct, cr)
                })
            } else {
                aL(ct, cr)
            }
        }

        function aL(ct, cu) {
            if (typeof cu !== "undefined") {
                var cs = ct.find(e);
                var cr = au(cu, ct);
                if (cr.length) {
                    bq(cs, cr)
                }
            }
        }

        function aP(ct, cs) {
            ct.append('<div class="' + p + '"><ul></ul></div>');
            var cu = ct.find(o);
            cu.addClass(bo.slidesNavPosition);
            for (var cr = 0; cr < cs; cr++) {
                cu.find("ul").append('<li><a href="#"><span></span></a></li>')
            }
            cu.css("margin-left", "-" + (cu.width() / 2) + "px");
            cu.find("li").first().find("a").addClass(H)
        }

        function bL(cu, cv, cr, ct) {
            var cs = "";
            if (bo.anchors.length && !bo.lockAnchors) {
                if (cu) {
                    if (typeof cr !== "undefined") {
                        cs = cr
                    }
                    if (typeof cv === "undefined") {
                        cv = cu
                    }
                    bp = cv;
                    ay(cs + "/" + cv)
                } else {
                    if (typeof cu !== "undefined") {
                        bp = cv;
                        ay(cr)
                    } else {
                        ay(cr)
                    }
                }
            }
            aX()
        }

        function ay(cr) {
            if (bo.recordHistory) {
                location.hash = cr
            } else {
                if (at || al) {
                    aa.history.replaceState(s, s, "#" + cr)
                } else {
                    var cs = aa.location.href.split("#")[0];
                    aa.location.replace(cs + "#" + cr)
                }
            }
        }

        function bY(ct) {
            var cs = ct.data("anchor");
            var cr = ct.index();
            if (typeof cs === "undefined") {
                cs = cr
            }
            return cs
        }

        function aX() {
            var cu = S(c);
            var cr = cu.find(k);
            var ct = bY(cu);
            var cw = bY(cr);
            var cv = String(ct);
            if (cr.length) {
                cv = cv + "-" + cw
            }
            cv = cv.replace("/", "-").replace("#", "");
            var cs = new RegExp("\\b\\s?" + P + "-[^\\s]+\\b", "g");
            a7[0].className = a7[0].className.replace(cs, "");
            a7.addClass(P + "-" + cv)
        }

        function bx() {
            var ct = g.createElement("p"),
                cu, cs = {
                    webkitTransform: "-webkit-transform",
                    OTransform: "-o-transform",
                    msTransform: "-ms-transform",
                    MozTransform: "-moz-transform",
                    transform: "transform"
                };
            g.body.insertBefore(ct, null);
            for (var cr in cs) {
                if (ct.style[cr] !== s) {
                    ct.style[cr] = "translate3d(1px,1px,1px)";
                    cu = aa.getComputedStyle(ct).getPropertyValue(cs[cr])
                }
            }
            g.body.removeChild(ct);
            return (cu !== s && cu.length > 0 && cu !== "none")
        }

        function aZ() {
            if (g.addEventListener) {
                g.removeEventListener("mousewheel", a8, false);
                g.removeEventListener("wheel", a8, false);
                g.removeEventListener("MozMousePixelScroll", a8, false)
            } else {
                g.detachEvent("onmousewheel", a8)
            }
        }

        function ao() {
            var ct = "";
            var cs;
            if (aa.addEventListener) {
                cs = "addEventListener"
            } else {
                cs = "attachEvent";
                ct = "on"
            }
            var cr = "onwheel" in g.createElement("div") ? "wheel" : g.onmousewheel !== s ? "mousewheel" : "DOMMouseScroll";
            if (cr == "DOMMouseScroll") {
                g[cs](ct + "MozMousePixelScroll", a8, false)
            } else {
                g[cs](ct + cr, a8, false)
            }
        }

        function cl() {
            bF.on("mousedown", ap).on("mouseup", aN)
        }

        function co() {
            bF.off("mousedown", ap).off("mouseup", aN)
        }

        function bE() {
            if (at || al) {
                var cr = aw();
                S(F).off("touchstart " + cr.down).on("touchstart " + cr.down, cp).off("touchmove " + cr.move).on("touchmove " + cr.move, b3)
            }
        }

        function bt() {
            if (at || al) {
                var cr = aw();
                S(F).off("touchstart " + cr.down).off("touchmove " + cr.move)
            }
        }

        function aw() {
            var cr;
            if (aa.PointerEvent) {
                cr = {
                    down: "pointerdown",
                    move: "pointermove"
                }
            } else {
                cr = {
                    down: "MSPointerDown",
                    move: "MSPointerMove"
                }
            }
            return cr
        }

        function am(cs) {
            var cr = [];
            cr.y = (typeof cs.pageY !== "undefined" && (cs.pageY || cs.pageX) ? cs.pageY : cs.touches[0].pageY);
            cr.x = (typeof cs.pageX !== "undefined" && (cs.pageY || cs.pageX) ? cs.pageX : cs.touches[0].pageX);
            if (al && cc(cs) && bo.scrollBar) {
                cr.y = cs.touches[0].pageY;
                cr.x = cs.touches[0].pageX
            }
            return cr
        }

        function a5(cs, cr) {
            aG.setScrollingSpeed(0, "internal");
            if (typeof cr !== "undefined") {
                aT = true
            }
            bq(cs.closest(e), cs);
            if (typeof cr !== "undefined") {
                aT = false
            }
            aG.setScrollingSpeed(bk.scrollingSpeed, "internal")
        }

        function bM(cs) {
            if (bo.scrollBar) {
                bF.scrollTop(cs)
            } else {
                if (bo.css3) {
                    var cr = "translate3d(0px, -" + cs + "px, 0px)";
                    bv(cr, false)
                } else {
                    bF.css("top", -cs)
                }
            }
        }

        function bJ(cr) {
            return {
                "-webkit-transform": cr,
                "-moz-transform": cr,
                "-ms-transform": cr,
                transform: cr
            }
        }

        function aJ(cs, ct, cr) {
            switch (ct) {
                case "up":
                    aE[cr].up = cs;
                    break;
                case "down":
                    aE[cr].down = cs;
                    break;
                case "left":
                    aE[cr].left = cs;
                    break;
                case "right":
                    aE[cr].right = cs;
                    break;
                case "all":
                    if (cr == "m") {
                        aG.setAllowScrolling(cs)
                    } else {
                        aG.setKeyboardScrolling(cs)
                    }
            }
        }
        aG.destroy = function(cr) {
            aG.setAutoScrolling(false, "internal");
            aG.setAllowScrolling(false);
            aG.setKeyboardScrolling(false);
            bF.addClass(D);
            clearTimeout(bl);
            clearTimeout(a0);
            clearTimeout(aj);
            clearTimeout(cn);
            clearTimeout(bG);
            ac.off("scroll", bT).off("hashchange", aI).off("resize", aQ);
            K.off("click", O + " a").off("mouseenter", O + " li").off("mouseleave", O + " li").off("click", d).off("mouseover", bo.normalScrollElements).off("mouseout", bo.normalScrollElements);
            S(C).off("click", U);
            clearTimeout(bl);
            clearTimeout(a0);
            if (cr) {
                aF()
            }
        };

        function aF() {
            bM(0);
            bF.find("img[data-src], source[data-src], audio[data-src], iframe[data-src]").each(function() {
                S(this).attr("src", S(this).data("src"));
                S(this).removeAttr("data-src")
            });
            S(O + ", " + o + ", " + U).remove();
            S(C).css({
                height: "",
                "background-color": "",
                padding: ""
            });
            S(Z).css({
                width: ""
            });
            bF.css({
                height: "",
                position: "",
                "-ms-touch-action": "",
                "touch-action": ""
            });
            bH.css({
                overflow: "",
                height: ""
            });
            S("html").removeClass(X);
            a7.removeClass(z);
            S(C + ", " + Z).each(function() {
                bo.scrollOverflowHandler.remove(S(this));
                S(this).removeClass(N + " " + H)
            });
            ca(bF);
            bF.find(u + ", " + J + ", " + e).each(function() {
                S(this).replaceWith(this.childNodes)
            });
            bH.scrollTop(0);
            var cr = [ah, f, y];
            S.each(cr, function(cs, ct) {
                S("." + ct).removeClass(ct)
            })
        }

        function b4(cr, ct, cs) {
            bo[cr] = ct;
            if (cs !== "internal") {
                bk[cr] = ct
            }
        }

        function b0() {
            if (S("html").hasClass(X)) {
                cd("error", "Fullpage.js can only be initialized once and you are doing it multiple times!");
                return
            }
            if (bo.continuousVertical && (bo.loopTop || bo.loopBottom)) {
                bo.continuousVertical = false;
                cd("warn", "Option `loopTop/loopBottom` is mutually exclusive with `continuousVertical`; `continuousVertical` disabled")
            }
            if (bo.scrollBar && bo.scrollOverflow) {
                cd("warn", "Option `scrollBar` is mutually exclusive with `scrollOverflow`. Sections with scrollOverflow might not work well in Firefox")
            }
            if (bo.continuousVertical && bo.scrollBar) {
                bo.continuousVertical = false;
                cd("warn", "Option `scrollBar` is mutually exclusive with `continuousVertical`; `continuousVertical` disabled")
            }
            S.each(bo.anchors, function(ct, cs) {
                var cr = K.find("[name]").filter(function() {
                    return S(this).attr("name") && S(this).attr("name").toLowerCase() == cs.toLowerCase()
                });
                var cu = K.find("[id]").filter(function() {
                    return S(this).attr("id") && S(this).attr("id").toLowerCase() == cs.toLowerCase()
                });
                if (cu.length || cr.length) {
                    cd("error", "data-anchor tags can not have the same value as any `id` element on the site (or `name` element for IE).");
                    cu.length && cd("error", '"' + cs + '" is is being used by another element `id` property');
                    cr.length && cd("error", '"' + cs + '" is is being used by another element `name` property')
                }
            })
        }

        function cd(cr, cs) {
            console && console[cr] && console[cr]("fullPage: " + cs)
        }
    };
    if (typeof IScroll !== "undefined") {
        IScroll.prototype.wheelOn = function() {
            this.wrapper.addEventListener("wheel", this);
            this.wrapper.addEventListener("mousewheel", this);
            this.wrapper.addEventListener("DOMMouseScroll", this)
        };
        IScroll.prototype.wheelOff = function() {
            this.wrapper.removeEventListener("wheel", this);
            this.wrapper.removeEventListener("mousewheel", this);
            this.wrapper.removeEventListener("DOMMouseScroll", this)
        }
    }
    var x = {
        refreshId: null,
        iScrollInstances: [],
        onLeave: function() {
            var aj = S(c).find(a).data("iscrollInstance");
            if (typeof aj !== "undefined" && aj) {
                aj.wheelOff()
            }
        },
        afterLoad: function() {
            var aj = S(c).find(a).data("iscrollInstance");
            if (typeof aj !== "undefined" && aj) {
                aj.wheelOn()
            }
        },
        create: function(aj, ak) {
            var al = aj.find(a);
            al.height(ak);
            al.each(function() {
                var an = jQuery(this);
                var am = an.data("iscrollInstance");
                if (am) {
                    S.each(x.iScrollInstances, function() {
                        S(this).destroy()
                    })
                }
                am = new IScroll(an.get(0), n);
                x.iScrollInstances.push(am);
                an.data("iscrollInstance", am)
            })
        },
        isScrolled: function(ak, al) {
            var aj = al.data("iscrollInstance");
            if (!aj) {
                return true
            }
            if (ak === "top") {
                return aj.y >= 0 && !al.scrollTop()
            } else {
                if (ak === "bottom") {
                    return (0 - aj.y) + al.scrollTop() + 1 + al.innerHeight() >= al[0].scrollHeight
                }
            }
        },
        scrollable: function(aj) {
            if (aj.find(e).length) {
                return aj.find(k).find(a)
            }
            return aj.find(a)
        },
        scrollHeight: function(aj) {
            return aj.find(a).children().first().get(0).scrollHeight
        },
        remove: function(ak) {
            var al = ak.find(a);
            if (al.length) {
                var aj = al.data("iscrollInstance");
                aj.destroy();
                al.data("iscrollInstance", null)
            }
            ak.find(a).children().first().children().first().unwrap().unwrap()
        },
        update: function(aj, ak) {
            clearTimeout(x.refreshId);
            x.refreshId = setTimeout(function() {
                S.each(x.iScrollInstances, function() {
                    S(this).get(0).refresh()
                })
            }, 150);
            aj.find(a).css("height", ak + "px").parent().css("height", ak + "px")
        },
        wrapContent: function() {
            return '<div class="' + w + '"><div class="fp-scroller"></div></div>'
        }
    }
});