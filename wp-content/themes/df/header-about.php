<!DOCTYPE html>
<html <?php language_attributes(); ?> class='no-js'>
<head>
<meta charset='<?php bloginfo( 'charset' ); ?>'>
<meta name='viewport' content="width=device-width, initial-scale=1">
<link rel='profile' href='http://gmpg.org/xfn/11'>
<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
<link rel='pingback' href='<?php bloginfo( 'pingback_url' ); ?>'>
<?php endif; ?>
<link href='<?php bloginfo('template_url');?>/css/bootstrap.min.css' rel='stylesheet'>
<link href='<?php bloginfo('template_url');?>/css/animate.css' rel='stylesheet'>
<link href='<?php bloginfo('template_url');?>/css/hover.css' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Cantarell" rel='stylesheet'>
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
<script src='<?php bloginfo('template_url');?>/js/jquery-3.1.0.min.js'></script>
<script src='<?php bloginfo('template_url');?>/js/typed.js'></script>
<script src='<?php bloginfo('template_url');?>/js/bootstrap.min.js'></script>
<script src='<?php bloginfo('template_url');?>/js/about_main.js'></script>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="main_wrapper">
<div class="wrapper">
<div class="container-fluid id=nav_scroll">
<div class="row home_slider_menu">
<div class="col-md-6">
<a href='<?php echo esc_url( home_url( '/' ) ); ?>'><img src='<?php bloginfo('template_url');?>/images/Logo-Wheel-Black.png' alt='logo' title="Design Foundary" class="home_logo"/></a>
</div>
<div class="col-md-6">
<a href="#" class="menu_btn">
<span style="background-color:#000"></span>
<span style="background-color:#000"></span>
<span style="background-color:#000"></span>
</a>
</div>
</div>
</div>
<div id="menu_section">
<img src='<?php bloginfo('template_url');?>/images/menu_bg.jpg' alt="" title="" class="menu_bg">
<div class="container-fluid">
<div class="row home_slider_menu">
<div class="col-md-6">
<a href='<?php echo esc_url( home_url( '/' ) ); ?>'><img src='<?php bloginfo('template_url');?>/images/Logo-Wheel-White.png' alt='logo' title="Design Foundary" class="home_logo"/></a>
</div>
<div class="col-md-6">
<div class="close-btn">
<a><span class="btn_close"></span></a>
</div>
</div>
</div>
<div class="col-md-12">
<div class="menu_list">
<?php wp_nav_menu( array('menu' => 'Main') );?>
<div class="social_icons">
<a href="https://www.facebook.com/DesignFoundryEvents/?fref=ts" target="_blank"><i class="fa fa-facebook" aria-hidden=true></i></a>
<a href='https://www.instagram.com/designfoundry/'><i class="fa fa-instagram" aria-hidden=true></i></a>
<a href="#"><i class="fa fa-twitter" aria-hidden=true></i></a>
</div>
</div>
</div>
</div>
</div>
