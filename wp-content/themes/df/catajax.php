<?php include('../../../wp-config.php');
//echo $_POST['dropcatid'];
//exit;
?>

	<script>
	$(document).ready(function(){

        $(".item").hover(function(){
			$(this).find(".ports_img").animate({top: "-20px"});

            $(this).find(".details").slideDown(300)
        
    },function(){
			$(this).find(".ports_img").animate({top: "0px"});
            $(this).find(".details").slideUp(300)
    });
    });


	      $("#typed_work_2").typed({
            // strings: ["Typed.js is a <strong>jQuery</strong> plugin.", "It <em>types</em> out sentences.", "And then deletes them.", "Try it out!"],
            stringsElement: $('#typed-strings_work_2'),
            typeSpeed: 5,
            backDelay: 3000,
            loop: true,
            contentType: 'html', // or text
            // defaults to false for infinite loop
            loopCount: false,
            callback: function(){ foo(); },
            resetCallback: function() { newTyped(); }
        });

        $(".reset").click(function(){
            $("#typed_work_2").typed('reset');
        });

	</script>


<div id="ajax_portfolio_page">
	<div class="container-fluid">
<div class="row">
			<div class="col-md-12 col-sm-12 text-center">
				<div class="work_typed">
					<div id="typed-strings_work_2">
						<h2 class="">I'm hosting a ...</h2>
					</div>
					 <span id="typed_work_2" style="white-space:pre;"></span>
				</div>
			    <div class="clearfix"></div>
				 
				 <div class="dropdown">
				
					  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> <?php if(!empty($_POST['dropcatid']))echo get_cat_name($_POST['dropcatid']);else echo "All Events"; ?>
					  <span class="glyphicon glyphicon-chevron-down"></span></button>
					  
						<?php
				$args = array(
					'type'                     => 'post',
					'child_of'                 => 0,
					'parent'                   => '',
					'orderby'                  => 'name',
					'order'                    => 'ASC',
					'hide_empty'               => 1,
					'hierarchical'             => 1,
					'exclude'                  => '',
					'include'                  => '',
					'number'                   => '',
					'post_type'                 => 'portfolio',
					'pad_counts'               => false );
				$categories = get_categories($args);
				
					echo '<ul class="dropdown-menu">';?>
					<li><a onclick="get_infoajax('0')" href="javascript::void(0);">All Events</a></li>
						<?php foreach ($categories as $category) {
                            //print_r($category);
                            ?>
                           <?php  //echo $category->term_id;?>
							<li><a onclick="get_infoajax(<?php echo $category->term_id; ?>)" href="javascript::void(0);"><?php echo $category->name; ?></a></li>
						<?php
						}
					
					echo '</ul>';
			?>
			
			<div id='loadingmessage' style='display:none'>
				<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
			</div>
					 </div>
			</div>
		</div>  <!-- end of the row -->
				<?php 
				if($_POST['dropcatid']=='0')
				{
			       $pf_query = new WP_Query(
                            array('post_type' => 'portfolio',
                                 //'category__in' => array($_POST['dropcatid']),
                                  'order' => 'ASC',
                                  'posts_per_page' => '-1',
                                  
                            )
                        );
				}
				else
				{
			         $pf_query = new WP_Query(
                            array('post_type' => 'portfolio',
                                 'category__in' => array($_POST['dropcatid']),
                                  'order' => 'ASC',
                                  'posts_per_page' => '-1',
                                  
                            )
                        );
				}
                
				?>
				
		<div class="row port_images">
			<?php //$content = get_the_content();
				while ($pf_query->have_posts()) : $pf_query->the_post();
				$pflink = get_permalink( $id );
				$the_link = get_permalink();
				$title = get_the_title(); 
				$content =  get_the_content();
				$terms = get_the_terms($post->ID, 'category' );
			   ?>
			<div class="col-md-4 col-sm-4 no_pad_port">
			<div class="item">
					<?php the_post_thumbnail('full', array('class' => 'img-responsive ports_img')); ?>
					<span class="details">
						<a href="<?php echo the_permalink();?>"><h6 class="comp_name"><?php $comp_name= the_field('company_name');?><?php echo $comp_name;?></h6></a>
						<h3><a href="<?php echo the_permalink();?>"><?php echo $title;?></a></h3>
						<a href="<?php echo the_permalink();?>"><h6 class="cat_name">
						<?php $category = get_the_category( $post->ID );
								echo $category[0]->cat_name;?></h6></a>
						
					</span>
				</div>
				
			</div>
			<?php endwhile; ?>
		</div>     <!-- end of the port_images row -->
		

		
	</div>     <!--  end of the containter-fluid  -->

	

</div>   <!-- end of the portfolio_page -->


