<?php

/*

 Template Name: casestudy

 */





get_header('casestudy'); ?>

<div class="casestudy_page">

<?php if(is_page('case-study')){?>


<?php        // $postatitlearray=array();  

				$pf_query = new WP_Query(array('post_type' =>'portfolio',                               

												'order' => 'ASC',

												'posts_per_page' => '-1',

												));  

						while ($pf_query->have_posts()) {

							$pf_query->the_post();?>

<?php global $post;



				$postatitlearray[] = $post->post_name;

				?>

<?php } 

				wp_reset_postdata();

					?>

<?php

				

					if( have_rows('banner') ):

					$x = 1;

					// loop through the rows of data

					while ( have_rows('banner') ) : the_row();

					$slider = get_sub_field('home_slider');

					$title = get_sub_field('title');

					?>



<section id="page_<?php echo $x;?>" class="section">



<img src="<?php echo $slider['url']; ?>" class="slider_img_align" alt="" title=""/>



<div class="slider_text">



<div class="row google_bg<?php echo $x;?>">

<div class="col-md-12">

<div class="span-group">

<div id="typed-strings_<?php echo $x;?>">

<h1><?php echo $title; ?></h1>

</div>

<span id="typed_<?php echo $x;?>" style="white-space:pre"></span>

</div>

<?php $stringtitle = str_replace(' ', '-', strtolower($title));?>

<?php if(in_array($stringtitle,$postatitlearray)){



												

												?>

<a class="btn" href="<?php echo $stringtitle;?>">VIEW EVENT</a>

<?php }?>

</div>

</div>



</div>

</section>

<?php $x++; endwhile;



				else :



					// no rows found



				endif;?>

</div>

</div>



<?php } else {?>

<?php 

if ( have_posts() ) {

	while ( have_posts() ) {

		the_post(); 

		the_content();

	} // end while

} // end if

?>



<?php }?>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>